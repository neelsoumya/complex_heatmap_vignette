#####################################################################################################################
# Vignettes and examples for
#   heatmaps using the
#   ComplexHeatmap package
# 
# Adapted from:
# https://bioconductor.org/packages/release/bioc/vignettes/ComplexHeatmap/inst/doc/s9.examples.html
#
#####################################################################################################################



###############################
# Load libraries
###############################
library(ComplexHeatmap)
library(circlize)

###############################
# Load example data
###############################
expr = readRDS(paste0(system.file(package = "ComplexHeatmap"), "/extdata/gene_expression.rds"))
mat = as.matrix(expr[, grep("cell", colnames(expr))])
base_mean = rowMeans(mat)


###############################
# Perform row scaling (zscore)
###############################
mat_scaled = t(apply(mat, 1, scale))

########################################
# Get column names from data frame
#  and use them as column annotations
########################################
type = gsub("s\\d+_", "", colnames(mat))
ha = HeatmapAnnotation(df = data.frame(type = type))

###############################
# Plot heatmap
###############################
Heatmap(mat_scaled, name = "expression", km = 5, col = colorRamp2(c(-2, 0, 2), c("green", "white", "red")),
        top_annotation = ha, top_annotation_height = unit(4, "mm"), 
        show_row_names = FALSE, show_column_names = FALSE)


###################################
# Plot two heatmaps side by side
###################################

mat1 = matrix(rnorm(80, 2), 8, 10)
mat1 = rbind(mat1, matrix(rnorm(40, -2), 4, 10))
rownames(mat1) = paste0("R", 1:12)
colnames(mat1) = paste0("C", 1:10)

mat2 = matrix(rnorm(60, 2), 6, 10)
mat2 = rbind(mat2, matrix(rnorm(60, -2), 6, 10))
rownames(mat2) = paste0("R", 1:12)
colnames(mat2) = paste0("C", 1:10)

ht1 = Heatmap(mat1, name = "ht1")
ht2 = Heatmap(mat2, name = "ht2")
ht1 + ht2



####################################
# Split rows of heatmap
####################################
Heatmap(mat1, name = "foo", split = rep(c("A", "B"), 6))



####################################
# Boxplot annotations on columns
####################################

ha_boxplot = HeatmapAnnotation(boxplot = anno_boxplot(mat_scaled))

Heatmap(mat_scaled, name = "expression", km = 5, 
          col = colorRamp2(c(-2, 0, 2), c("green", "white", "red")),
          top_annotation = ha_boxplot, 
          top_annotation_height = unit(4, "mm"), 
          show_row_names = FALSE, 
          show_column_names = FALSE)


####################################
# Better colors for complex heatmap
####################################

# color scheme 1 (diverging palette) 
# RdYlBu
# see https://betterfigures.org/2015/06/23/picking-a-colour-scale-for-scientific-graphics/
Heatmap(mat_scaled, name = "expression", km = 5, 
        col = colorRampPalette(rev(brewer.pal(n=11, name="RdYlBu")))(100),
        top_annotation = ha_boxplot, 
        top_annotation_height = unit(4, "mm"), 
        show_row_names = FALSE, 
        show_column_names = FALSE)

# color scheme 2 (diverging palette) 
# RdBu
# see https://betterfigures.org/2015/06/23/picking-a-colour-scale-for-scientific-graphics/
i_num_colours = 8
Heatmap(mat_scaled, name = "expression", km = 5, 
                col = colorRamp2(
                  seq(
                    min(mat_scaled),
                    max(mat_scaled),
                    length.out=i_num_colours),
                  rev(brewer.pal(n=i_num_colours, name="RdBu")), 
                  transparency = 0.1, 
                  space = "LAB"),
        top_annotation = ha_boxplot, 
        top_annotation_height = unit(4, "mm"), 
        show_row_names = FALSE, 
        show_column_names = FALSE)


####################################
# Multiple column annotations
#   and barplots
#   Use densityHeatmap
####################################

ha_multi = HeatmapAnnotation(df = data.frame(anno = rep(c("A", "B"), each = 5)),
                       col = list(anno = c("A" = "green", "B" = "orange")),
                       points = anno_points(runif(10)))

densityHeatmap(mat_scaled, 
               anno = ha_multi)



####################################
# Color bars for ComplexHeatmap
####################################
df = data.frame(type = c(rep("a", 5), rep("b", 5)))

ha = HeatmapAnnotation(df = df, 
                       col = list(type = c("a" =  "red", "b" = "blue"))
                       )

Heatmap(mat_scaled, top_annotation = ha)


######################################################
# Even prettier looking ComplexHeatmap
# courtesy Kevin Rue-Albrecht
#   in the style of Morpheus
# also
# https://github.com/jokergoo/ComplexHeatmap/issues/77
######################################################

i_num_colours = 10
# without column clustering
Heatmap(mat_scaled,
        #col=colorRampPalette(rev(brewer.pal(n=11, name="RdYlBu")))(30) ,
        #col=colorRampPalette(c("blue","white","red"))(20) ,
        col = colorRamp2(
          seq(
            min(mat_scaled),
            max(mat_scaled),
            length.out=i_num_colours),
          rev(brewer.pal(n=i_num_colours, name="RdBu")), 
          transparency = 0.1, 
          space = "LAB"),
        #gap = unit(1, "mm"),
        # split = rep(c("A", "B", "C", "D"),61 ),
        #split = all_split_delimiter,
        show_column_names = TRUE,
        cluster_columns = FALSE,
        rect_gp = gpar(col = "black", lty = 1, lwd = 0.2),
        show_row_names = FALSE#,
        #cluster_columns = den_opt
)

Heatmap(mat_scaled,
        #col=colorRampPalette(rev(brewer.pal(n=11, name="RdYlBu")))(30) ,
        col=colorRampPalette(c("blue","white","red"))(20) ,
        # col = colorRamp2(
        #   seq(
        #     min(mat_scaled),
        #     max(mat_scaled),
        #     length.out=i_num_colours),
        #   rev(brewer.pal(n=i_num_colours, name="RdBu")), 
        #   transparency = 0.1, 
        #   space = "LAB"),
        #gap = unit(1, "mm"),
        # split = rep(c("A", "B", "C", "D"),61 ),
        #split = all_split_delimiter,
        show_column_names = TRUE,
        cluster_columns = FALSE,
        rect_gp = gpar(col = "black", lty = 1, lwd = 0.2),
        show_row_names = FALSE#,
        #cluster_columns = den_opt
)


# with column clustering
Heatmap(mat_scaled,
        #col=colorRampPalette(rev(brewer.pal(n=11, name="RdYlBu")))(30) ,
        #col=colorRampPalette(c("blue","white","red"))(20) ,
        col = colorRamp2(
          seq(
            min(mat_scaled),
            max(mat_scaled),
            length.out=i_num_colours),
          rev(brewer.pal(n=i_num_colours, name="RdBu")), 
          transparency = 0.1, 
          space = "LAB"),
        #gap = unit(1, "mm"),
        # split = rep(c("A", "B", "C", "D"),61 ),
        #split = all_split_delimiter,
        show_column_names = TRUE,
        #cluster_columns = FALSE,
        rect_gp = gpar(col = "black", lty = 1, lwd = 0.2),
        show_row_names = FALSE#,
        #cluster_columns = den_opt
)

Heatmap(mat_scaled,
        #col=colorRampPalette(rev(brewer.pal(n=11, name="RdYlBu")))(30) ,
        col=colorRampPalette(c("blue","white","red"))(20) ,
        # col = colorRamp2(
        #   seq(
        #     min(mat_scaled),
        #     max(mat_scaled),
        #     length.out=i_num_colours),
        #   rev(brewer.pal(n=i_num_colours, name="RdBu")), 
        #   transparency = 0.1, 
        #   space = "LAB"),
        #gap = unit(1, "mm"),
        # split = rep(c("A", "B", "C", "D"),61 ),
        #split = all_split_delimiter,
        show_column_names = TRUE,
        #cluster_columns = FALSE,
        rect_gp = gpar(col = "black", lty = 1, lwd = 0.2),
        show_row_names = FALSE#,
        #cluster_columns = den_opt
)
